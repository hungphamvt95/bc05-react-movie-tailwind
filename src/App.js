import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div>
      <h2 className="text-red-500 font-medium text-center">Hello Cybersoft</h2>
    </div>
  );
}

export default App;
